﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelPanel : MonoBehaviour
{
    public int totalLevelCount;
    public Button levelButton;
    public Transform levelGridPanel;

    private int currentUserLevel;

    private void Start()
    {
        if(MySelectedCarAndLevel.Instance.timeMode == false)
        {
            currentUserLevel = PlayerPrefs.GetInt("UserLevelNormalGame");
        }
        else
        {
            currentUserLevel = PlayerPrefs.GetInt("UserLevelLimitedTime");
        }
        createNormalGameLevelButtons();
    }

    private void createNormalGameLevelButtons()
    {
        for(int i=0; i < totalLevelCount; i++)
        {
            Button b = Instantiate(levelButton, levelGridPanel,false);

            // Set level of button for opening its level
            b.GetComponent<LevelButtonScript>().buttonLevel = i+1;

            // If user level is not enough, lock the level button
            if(i < currentUserLevel)
            {
                b.interactable = true;
            }
            else
            {
                b.interactable = false;
            }
        }
    }


}
