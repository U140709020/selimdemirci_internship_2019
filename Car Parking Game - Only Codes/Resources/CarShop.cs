﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarShop : MonoBehaviour
{

    public GameObject warningText;

    // This script for buying car and saving purchased car, it include camera animations

    public void buyANewCar()
    {
        if(CarSelection.Instance.selectedCar.GetComponent<CarInfo>().priceCurrency == "GOLD")
        {
            if(BudgetManager.Instance.goldBudget >= CarSelection.Instance.selectedCar.GetComponent<CarInfo>().carPrice)
            {
                BudgetManager.Instance.spendGold(CarSelection.Instance.selectedCar.GetComponent<CarInfo>().carPrice);
                GoldBudgetText.Instance.showGoldBudget();

                CarSelection.Instance.hideAndShowButtonsForPurchasedCar();

                PlayerPrefs.SetString(CarSelection.Instance.selectedCar.GetComponent<CarInfo>().carName, "Have");
                PlayerPrefs.Save();

                CarPriceText.Instance.closeTextForPurchasedCars();

                startCamAnimation();

                Invoke("backToInitialStateCamera", 2f);

            }
            else
            {
                warningText.SetActive(true);
                Invoke("closeWarningText", 1.5f);
            }
        }
        else
        {
            if (BudgetManager.Instance.dollarBudget >= CarSelection.Instance.selectedCar.GetComponent<CarInfo>().carPrice)
            {
                BudgetManager.Instance.spendDollar(CarSelection.Instance.selectedCar.GetComponent<CarInfo>().carPrice);
                DollarBudgetText.Instance.showDollarBudget();

                CarSelection.Instance.hideAndShowButtonsForPurchasedCar();

                PlayerPrefs.SetString(CarSelection.Instance.selectedCar.GetComponent<CarInfo>().carName, "Have");
                PlayerPrefs.Save();

                CarPriceText.Instance.closeTextForPurchasedCars();

                startCamAnimation();

                Invoke("backToInitialStateCamera", 2f);
            }
            else
            {
                warningText.SetActive(true);
                Invoke("closeWarningText", 1.5f);
            }
        }

    }

    private void closeWarningText()
    {
        warningText.SetActive(false);
    }

    private void backToInitialStateCamera()
    {
        Camera.main.GetComponent<Animator>().enabled = true;
        Camera.main.GetComponent<Animator>().Play("Cam02");
        Invoke("closeCamAnimation", 1.1f);
        CarSelection.Instance.bringBackObjectAfterPurchasedCarAnimation();
    }

    private void startCamAnimation()
    {
        Camera.main.GetComponent<Animator>().enabled = true;
        Camera.main.GetComponent<Animator>().Play("Cam01");
    }

    private void closeCamAnimation()
    {
        Camera.main.GetComponent<Animator>().enabled = false;
    }


}
