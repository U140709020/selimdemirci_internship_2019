﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DollarBudgetText : MonoBehaviour
{
    private static DollarBudgetText _instance;
    public static DollarBudgetText Instance { get { return _instance; } }


    public Text dollarBudgetText;


    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    void Start()
    {
        showDollarBudget();
        // Invoke("showDollarBudget",0.5f);
    }


    public void showDollarBudget()
    {
        dollarBudgetText.text = BudgetManager.Instance.dollarBudget + " " + LocalizationManager.Instance.GetText("DOLLAR");
    }

}
