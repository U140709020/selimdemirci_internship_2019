﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClosePopup : MonoBehaviour
{

    public string popupName;

    public void closeThisPopup()
    {
        CarSelection.Instance.bringBackObjectAfterPurchasedCarAnimation();
        Destroy(GameObject.FindWithTag(popupName));
    }


}
