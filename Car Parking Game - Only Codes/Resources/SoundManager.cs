﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : SingletonComponent<SoundManager>
{


    public AudioSource sound;

    public AudioClip soundClip;

    public bool soundChecker = true;


    public void Start()
    {
        DontDestroyOnLoad(gameObject);
        sound = gameObject.AddComponent<AudioSource>();
        sound.playOnAwake = false;
    }

    public void sensorSound(string sensorName)
    {
        if(soundChecker)
        {
            soundClip = (AudioClip)Resources.Load("Sounds/Parking Sensor/" + sensorName);
            sound.clip = soundClip;
            sound.loop = true;
            sound.Play();
        }
    }


    public void closeSensorSound()
    {
        sound.Stop();
    }
}
