﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DriveButton : MonoBehaviour
{

    public GameObject levelPopup;

    public void openNormalModeLevelPopup()
    {
        CarSelection.Instance.hideAndShowButtonsForPurchasedCar();
        MySelectedCarAndLevel.Instance.timeMode = false;
        Instantiate(levelPopup, GameObject.Find("Canvas").transform, false);
    }

    public void openTimeModeLevelPopup()
    {
        CarSelection.Instance.hideAndShowButtonsForPurchasedCar();
        MySelectedCarAndLevel.Instance.timeMode = true;
        Instantiate(levelPopup, GameObject.Find("Canvas").transform, false);
    }

}
