﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomizeScreenButtons : MonoBehaviour
{
    private static CustomizeScreenButtons _instance;
    public static CustomizeScreenButtons Instance { get { return _instance; } }

    public GameObject slidersPanel;
    public GameObject carSelectionPanel;
    public GameObject customizeButtonsPanel;

    public GameObject customizeButton;
    public GameObject useItemButton;
    public GameObject buyNewItemButton;
    public GameObject backButton;
    public GameObject driveNormalButton;
    public GameObject driveLimitedTimeButtn;
    public GameObject optionsButton;


    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }


    public void customizeButtonOnClick()
    {
        //GameObject go = Instantiate(CarSelection.Instance.selectedCar, new Vector3(-31.07317f, 8.92f, 28.25308f), CarSelection.Instance.selectedCar.transform.rotation) as GameObject;
        //go.transform.SetParent(customizingCar.transform, false);

        //CarSelection.Instance.selectedCar.SetActive(false);

        
        foreach(Transform t in customizeButtonsPanel.transform)
        {
            t.gameObject.SetActive(true);
        }

        slidersPanel.SetActive(false);
        carSelectionPanel.SetActive(false);
        customizeButton.SetActive(false);
        backButton.SetActive(true);
        driveNormalButton.SetActive(false);
        driveLimitedTimeButtn.SetActive(false);
        optionsButton.SetActive(false);
    }

    public void backButtonOnClick()
    {
        //Destroy(customizingCar.transform.GetChild(0).gameObject);
        //CarSelection.Instance.selectedCar.SetActive(true);

        CarSelection.Instance.selectedCar.GetComponent<CustomizedCar>().loadAllData();

        foreach (Transform t in customizeButtonsPanel.transform)
        {
            t.gameObject.SetActive(false);
        }

        useItemButton.SetActive(false);
        slidersPanel.SetActive(true);
        carSelectionPanel.SetActive(true);
        customizeButton.SetActive(true);
        buyNewItemButton.SetActive(false);
        backButton.SetActive(false);
        driveNormalButton.SetActive(true);
        driveLimitedTimeButtn.SetActive(true);
        optionsButton.SetActive(true);

    }


}

