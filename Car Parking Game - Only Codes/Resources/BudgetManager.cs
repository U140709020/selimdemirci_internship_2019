﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BudgetManager : SingletonComponent<BudgetManager>
{


    public int goldBudget;
    public int dollarBudget;

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);

        goldBudget = PlayerPrefs.GetInt("Gold");
        dollarBudget = PlayerPrefs.GetInt("Dollar");
    }


    public void spendGold(int sGold)
    {
        goldBudget = PlayerPrefs.GetInt("Gold") - sGold;
        PlayerPrefs.SetInt("Gold", goldBudget);
        PlayerPrefs.Save();
    }


    public void spendDollar(int sDollar)
    {
        dollarBudget = PlayerPrefs.GetInt("Dollar") - sDollar;
        PlayerPrefs.SetInt("Dollar", dollarBudget);
        PlayerPrefs.Save();
    }


    public void buyGold(int bGold)
    {
        goldBudget = PlayerPrefs.GetInt("Gold") + bGold;
        PlayerPrefs.SetInt("Gold", goldBudget);
        PlayerPrefs.Save();
    }

    public void buyDollar(int bDollar)
    {
        dollarBudget = PlayerPrefs.GetInt("Dollar") + bDollar;
        PlayerPrefs.SetInt("Dollar", dollarBudget);
        PlayerPrefs.Save();
    }

}
