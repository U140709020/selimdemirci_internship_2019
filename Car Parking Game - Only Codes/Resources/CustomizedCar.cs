﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomizedCar : MonoBehaviour
{

    private GameObject customizingButtonsObject;


    private Color defaultBodyColor;
    private Color defaultGlassColor;


    private void Start()
    {
        // All customizing scripts are in this object
        customizingButtonsObject = GameObject.Find("CustomizingButtons");

        // Save default color
        defaultBodyColor = gameObject.transform.Find("CarColor").GetComponent<Renderer>().material.color;
        defaultGlassColor = gameObject.transform.Find("Glass").GetComponent<Renderer>().material.color;
        loadAllData();
    }

    public void loadAllData()
    {
        loadBodyColor();
        loadGlassColor();
        loadWheels();
        loadSpoiler();
        loadWheelsColor();
    }



    // Loading saved data
    private void loadBodyColor()
    {
        if(PlayerPrefs.GetString(gameObject.GetComponent<CarInfo>().carName + "BODY_COLOR") == gameObject.GetComponent<CarInfo>().carName + "GREEN")
        {
            gameObject.transform.Find("CarColor").GetComponent<Renderer>().material.color = Color.green;
        }
        else if (PlayerPrefs.GetString(gameObject.GetComponent<CarInfo>().carName + "BODY_COLOR") == gameObject.GetComponent<CarInfo>().carName + "BLUE")
        {
            gameObject.transform.Find("CarColor").GetComponent<Renderer>().material.color = Color.blue;
        }
        else
        {
            gameObject.transform.Find("CarColor").GetComponent<Renderer>().material.color = defaultBodyColor;
        }
    }

    private void loadGlassColor()
    {
        if (PlayerPrefs.GetString(gameObject.GetComponent<CarInfo>().carName + "GLASS_COLOR") == gameObject.GetComponent<CarInfo>().carName + "CYAN")
        {
            gameObject.transform.Find("Glass").GetComponent<Renderer>().material.color = Color.cyan;
        }
        else if (PlayerPrefs.GetString(gameObject.GetComponent<CarInfo>().carName + "GLASS_COLOR") == gameObject.GetComponent<CarInfo>().carName + "MAGENTA")
        {
            gameObject.transform.Find("Glass").GetComponent<Renderer>().material.color = Color.magenta;
        }
        else
        {
            gameObject.transform.Find("Glass").GetComponent<Renderer>().material.color = defaultGlassColor;
        }
    }

    private void loadWheelsColor()
    {
        if (PlayerPrefs.GetString(PlayerPrefs.GetString(gameObject.GetComponent<CarInfo>().carName + "WHEELS") + "WHEELS_COLOR") == gameObject.GetComponent<CarInfo>().carName + "GREEN")
        {
            makeWheelsGreen(gameObject.transform.Find("Wheel Models").transform);
        }
        else if (PlayerPrefs.GetString(PlayerPrefs.GetString(gameObject.GetComponent<CarInfo>().carName + "WHEELS") + "WHEELS_COLOR") == gameObject.GetComponent<CarInfo>().carName + "RED")
        {
            makeWheelsRed(gameObject.transform.Find("Wheel Models").transform);
        }
        else
        {
            loadWheels();
        }
    }


    private void loadWheels()
    {
        if (PlayerPrefs.GetString(gameObject.GetComponent<CarInfo>().carName + "WHEELS") == gameObject.GetComponent<CarInfo>().carName + "C63")
        {
            RCC_Customization.ChangeWheels(gameObject.GetComponent<RCC_CarControllerV3>(), RCC_ChangableWheels.Instance.wheels[3].wheel);
        }
        else if (PlayerPrefs.GetString(gameObject.GetComponent<CarInfo>().carName + "WHEELS") == gameObject.GetComponent<CarInfo>().carName + "CHARG_RT")
        {
            RCC_Customization.ChangeWheels(gameObject.GetComponent<RCC_CarControllerV3>(), RCC_ChangableWheels.Instance.wheels[4].wheel);
        }
        else
        {
            // You have to add if statements according car name for getting their own default wheels, for now I used a wheel prefab as default wheels for every car
            RCC_Customization.ChangeWheels(gameObject.GetComponent<RCC_CarControllerV3>(), RCC_ChangableWheels.Instance.wheels[5].wheel);
        }
    }

    private void loadSpoiler()
    {
        Transform spoiler = gameObject.transform.Find("Spoiler");
        deleteLastSpoiler(spoiler);

        if (PlayerPrefs.GetString(gameObject.GetComponent<CarInfo>().carName + "SPOILER") == gameObject.GetComponent<CarInfo>().carName + "WING1")
        {
            closeDefaultSpoiler(gameObject.GetComponent<CarInfo>().carName,"SupraSpoiler");

            GameObject go = Instantiate(customizingButtonsObject.GetComponent<ChangeSpoiler>().wing1, new Vector3(0, 0, 0), Quaternion.Euler(new Vector3(0, -90, 0)));
            go.transform.SetParent(spoiler, false);
        }
        else if (PlayerPrefs.GetString(gameObject.GetComponent<CarInfo>().carName + "SPOILER") == gameObject.GetComponent<CarInfo>().carName + "WING2")
        {
            closeDefaultSpoiler(gameObject.GetComponent<CarInfo>().carName,"SupraSpoiler");

            GameObject go = Instantiate(customizingButtonsObject.GetComponent<ChangeSpoiler>().wing2, new Vector3(0, 0, 0), Quaternion.Euler(new Vector3(0, -90, 0)));
            go.transform.SetParent(spoiler, false);
        }
        else
        {
            // Load default spoilers

            if (gameObject.GetComponent<CarInfo>().carName == "SUPRA")
            {
                gameObject.transform.Find("SupraSpoiler").gameObject.SetActive(true);
            }

        }
    }

    private void deleteLastSpoiler(Transform t)
    {
        for (int i = 0; i < t.childCount; i++)
        {
            Destroy(t.GetChild(i).gameObject);
        }
    }

    private void closeDefaultSpoiler(string CarName, string DefaultSpoilerName)
    {
        // You can add cars if they have spoilers
        if (CarName == "SUPRA")
        {
            gameObject.transform.Find(DefaultSpoilerName).gameObject.SetActive(false);
        }
    }


    // Changing wheel colors
    public void makeWheelsRed(Transform t)
    {
        for (int i = 0; i < t.childCount; i++)
        {
            for (int z = 0; z < t.GetChild(i).transform.childCount; z++)
            {
                t.GetChild(i).transform.GetChild(z).gameObject.GetComponent<Renderer>().material.color = Color.red;
            }
        }
    }


    public void makeWheelsGreen(Transform t)
    {
        for (int i = 0; i < t.childCount; i++)
        {
            for (int z = 0; z < t.GetChild(i).transform.childCount; z++)
            {
                t.GetChild(i).transform.GetChild(z).gameObject.GetComponent<Renderer>().material.color = Color.green;
            }
        }
    }



}
