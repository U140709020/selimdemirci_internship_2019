﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelButtonScript : MonoBehaviour
{

    public int buttonLevel;

    private void Start()
    {
        gameObject.GetComponentInChildren<Text>().text = buttonLevel.ToString();
    }

    public void openLevel()
    {
        MySelectedCarAndLevel.Instance.selectedUserLevel = buttonLevel;
        Camera.main.GetComponent<Animator>().enabled = true;
        Camera.main.GetComponent<Animator>().Play("Cam01");

        // For hiding object from screen without destroying
        GameObject.FindWithTag("LevelPopup").transform.position = new Vector3(5555, 5555, 5555);
        Invoke("startGame", 1.5f);
    }

    private void startGame()
    {
        SceneManager.LoadScene("TrafficRulesDemoSceneNight");
    }

}
