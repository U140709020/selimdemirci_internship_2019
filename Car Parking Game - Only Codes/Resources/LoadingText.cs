﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadingText : MonoBehaviour
{

    public GameObject buttons;

    private void Start()
    {
        if (PlayerPrefs.HasKey("SelectedLanguage"))
        {
            buttons.SetActive(false);
            GetComponent<Text>().text = LocalizationManager.Instance.GetText("LOADING_SCREEN");
            Invoke("goMainMenu", 2.5f);
        }
        else
        {
            GetComponent<Text>().text = "Choose your language!";
        }
    }

    private void goMainMenu()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("1-MENU");
    }

}
