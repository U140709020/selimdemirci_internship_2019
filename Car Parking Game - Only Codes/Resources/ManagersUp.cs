﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagersUp : MonoBehaviour
{

    // In the very first scene, managers should be up
    private void Awake()
    {
        BudgetManager.Instance.enabled = true;
        LocalizationManager.Instance.enabled = true;
        SoundManager.Instance.enabled = true;

        if (PlayerPrefs.HasKey("FirstLogin") == false)
        {
            BudgetManager.Instance.buyGold(1750);
            BudgetManager.Instance.buyDollar(450000);

            PlayerPrefs.SetString("F488", "Have");
            PlayerPrefs.SetInt("FirstLogin", 1);
            PlayerPrefs.SetInt("UserLevelNormalGame", 1);
            PlayerPrefs.SetInt("UserLevelLimitedTime", 1);
            PlayerPrefs.Save();
        }
    }


}
