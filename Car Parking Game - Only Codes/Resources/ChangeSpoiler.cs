﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeSpoiler : MonoBehaviour {


    // Different brand's spoilers
    public GameObject wing1;
    public GameObject wing2;

    // Selected car's spoiler
    private Transform spoiler;


    // It deletes last spoiler
    private void DeleteLastSpoiler()
    {
        findCarSpoiler();

        for (int i = 0; i < spoiler.childCount; i++)
        {
            Destroy(spoiler.GetChild(i).gameObject);
        }
    }

    private void findCarSpoiler()
    {
        spoiler = CarSelection.Instance.selectedCar.transform.Find("Spoiler");
    }


    // Instantiates different spoilers
    public void createSpoilerWing1()
    {
        CustomizeManager.Instance.setItemInfo("Wing1", 100, "GOLD", CarSelection.Instance.selectedCarName + "SPOILER", "WING1");

        DeleteLastSpoiler();
        closeSupraDefaultSpoiler();

        GameObject go = Instantiate(wing1, new Vector3(0, 0, 0), Quaternion.Euler(new Vector3(0, -90, 0)));
        go.transform.SetParent(spoiler, false);
    }

    public void createSpoilerWing2()
    {
        CustomizeManager.Instance.setItemInfo("Wing2", 100, "GOLD", CarSelection.Instance.selectedCarName + "SPOILER", "WING2");

        DeleteLastSpoiler();
        closeSupraDefaultSpoiler();

        GameObject go = Instantiate(wing2, new Vector3(0, 0, 0), Quaternion.Euler(new Vector3(0, -90, 0)));
        go.transform.SetParent(spoiler, false);

    }


    private void closeSupraDefaultSpoiler()
    {
        if (CarSelection.Instance.selectedCarName == "SUPRA")
        {
            CarSelection.Instance.selectedCar.transform.Find("SupraSpoiler").gameObject.SetActive(false);
        }
    }

}
