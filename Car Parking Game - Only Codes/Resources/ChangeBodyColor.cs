﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeBodyColor : MonoBehaviour {


    private GameObject body;

    private void findBodyObject()
    {
        body = CarSelection.Instance.selectedCar.transform.Find("CarColor").gameObject;
    }

    // Changing body color
    public void makeBodyGreen()
    {
        CustomizeManager.Instance.setItemInfo("Body_green", 2500, "DOLLAR", CarSelection.Instance.selectedCarName + "BODY_COLOR","GREEN");

        findBodyObject();

        body.GetComponent<Renderer>().material.color = Color.green;

    }

    public void makeBodyBlue()
    {
        CustomizeManager.Instance.setItemInfo("Body_blue", 2500, "DOLLAR", CarSelection.Instance.selectedCarName + "BODY_COLOR", "BLUE");

        findBodyObject();

        body.GetComponent<Renderer>().material.color = Color.blue;

    }
}
