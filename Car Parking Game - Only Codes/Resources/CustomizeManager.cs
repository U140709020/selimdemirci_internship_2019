﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomizeManager : MonoBehaviour
{

    private static CustomizeManager _instance;
    public static CustomizeManager Instance { get { return _instance; } }

    public string currentObjectType;
    public string currentObjectName;

    public string itemName;
    public int itemPrice;
    public string itemPriceCurrency;

    public GameObject buyItemButton;
    public GameObject useItemButton;


    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

    }

    // For use item button on customization menu
    public void useThisObject()
    {
        PlayerPrefs.SetString(currentObjectType, currentObjectName);
        PlayerPrefs.Save();

        MySelectedCarAndLevel.Instance._mySelectedCar.GetComponent<CustomizedCar>().loadAllData();
    }

    public void setItemInfo(string name, int price, string currency, string objectType, string objectName)
    {
        // Customization different for every car with itemName value calling car name and item name and merging them
        itemName = CarSelection.Instance.selectedCarName + name;
        itemPrice = price;
        itemPriceCurrency = currency;
        currentObjectType = objectType;
        currentObjectName = CarSelection.Instance.selectedCarName + objectName;

        if (PlayerPrefs.HasKey(itemName))
        {
            useItemButton.SetActive(true);
            buyItemButton.SetActive(false);
        }
        else
        {
            useItemButton.SetActive(false);
            buyItemButton.SetActive(true);
        }
    }

}
