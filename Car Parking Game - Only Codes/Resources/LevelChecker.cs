﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelChecker : MonoBehaviour
{


    // There are 4 of them in a level object, if colliders triggered, checkPoints will be increase and if counts reach 4 then win the game
    private void OnTriggerEnter(Collider other)
    {
        // Debug.Log(other.name);
        LevelManager.Instance.checkPoints++;
        Invoke("check", 3f);
    }

    private void OnTriggerExit(Collider other)
    {
        if(LevelManager.Instance.checkPoints > 0)
        {
            LevelManager.Instance.checkPoints--;
        }
    }

    private void check()
    {
        LevelManager.Instance.checkLevelFinishOrNot();
    }
}
