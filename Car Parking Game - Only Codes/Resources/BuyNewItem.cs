﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuyNewItem : MonoBehaviour
{

    // For buy item button on customization menu
    public void buyThisItem()
    {
        if(CustomizeManager.Instance.itemPriceCurrency == "DOLLAR")
        {
            if (BudgetManager.Instance.dollarBudget >= CustomizeManager.Instance.itemPrice)
            {
                BudgetManager.Instance.spendDollar(CustomizeManager.Instance.itemPrice);
                DollarBudgetText.Instance.showDollarBudget();

                PlayerPrefs.SetString(CustomizeManager.Instance.itemName, "Have");
                PlayerPrefs.Save();
            }

        }
        else
        {
            if (BudgetManager.Instance.goldBudget >= CustomizeManager.Instance.itemPrice)
            {
                BudgetManager.Instance.spendGold(CustomizeManager.Instance.itemPrice);
                GoldBudgetText.Instance.showGoldBudget();

                PlayerPrefs.SetString(CustomizeManager.Instance.itemName, "Have");
                PlayerPrefs.Save();
            }
        }
        gameObject.SetActive(false);
        CustomizeManager.Instance.useItemButton.SetActive(true);

    }


}
