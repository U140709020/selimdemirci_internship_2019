﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarInfo : MonoBehaviour
{


    public float speed;
    public float acceleration;
    public float breakC;

    public int carPrice;
    public string priceCurrency;

    public string carName;

    private bool reverseGearOn = false;

    private void Start()
    {
        DontDestroyOnLoad(this);
        RCC.SetEngine(gameObject.GetComponent<RCC_CarControllerV3>(), false);
    }


    // If reverse gear reverse sound on, if not off
    private void Update()
    {
        if(gameObject.GetComponent<RCC_CarControllerV3>().direction == -1 && !reverseGearOn)
        {
            SoundManager.Instance.sensorSound("sensor4");
            reverseGearOn = true;
        }
        else if(gameObject.GetComponent<RCC_CarControllerV3>().direction == 1 && reverseGearOn)
        {
            SoundManager.Instance.closeSensorSound();
            reverseGearOn = false;
        }
    }




}
