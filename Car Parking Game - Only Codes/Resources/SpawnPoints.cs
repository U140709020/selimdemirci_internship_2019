﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPoints : MonoBehaviour
{



    private void Start()
    {
        gameObject.GetComponent<Renderer>().material.color = Color.yellow;
        spawnTheCar();
    }


    // Spawn car with position and rotation on City Scene
    private void spawnTheCar()
    {
        MySelectedCarAndLevel.Instance._mySelectedCar.transform.position = LevelManager.Instance.spawnPointsByLevel[LevelManager.Instance.userLevel-1];
        MySelectedCarAndLevel.Instance._mySelectedCar.transform.eulerAngles = LevelManager.Instance.spawnRotatesByLevel[LevelManager.Instance.userLevel - 1];
    }




}
