﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeWheels : MonoBehaviour {


    // Wheels transform panel
    public Transform wheelPanel;

    // Finding the car's wheels and adding them to list
    private void findMyCar()
    {
        wheelPanel = CarSelection.Instance.selectedCar.transform.Find("Wheel Models");
    }


    // Changing wheels
    public void wheels_c63()
    {
        CustomizeManager.Instance.setItemInfo("Wheel_c63", 100, "GOLD", CarSelection.Instance.selectedCarName + "WHEELS", "C63");

        RCC_Customization.ChangeWheels(CarSelection.Instance.selectedCar.GetComponent<RCC_CarControllerV3>(), RCC_ChangableWheels.Instance.wheels[3].wheel);
    }



    public void wheels_charg_rt()
    {
        CustomizeManager.Instance.setItemInfo("Wheel_charg_rt", 100, "GOLD", CarSelection.Instance.selectedCarName + "WHEELS", "CHARG_RT");

        RCC_Customization.ChangeWheels(CarSelection.Instance.selectedCar.GetComponent<RCC_CarControllerV3>(), RCC_ChangableWheels.Instance.wheels[4].wheel);

    }



    // Changing wheel colors
    public void makeWheelsRed()
    {
        CustomizeManager.Instance.setItemInfo("Wheel_red", 2500, "DOLLAR", PlayerPrefs.GetString(CarSelection.Instance.selectedCarName + "WHEELS") + "WHEELS_COLOR", "RED");

        findMyCar();

        for (int i = 0; i < wheelPanel.childCount; i++)
        {
            for (int z = 0; z < wheelPanel.GetChild(i).transform.childCount; z++)
            {
                wheelPanel.GetChild(i).transform.GetChild(z).gameObject.GetComponent<Renderer>().material.color = Color.red;
            }
        }

    }


    public void makeWheelsGreen()
    {
        CustomizeManager.Instance.setItemInfo("Wheel_green", 2500, "DOLLAR", PlayerPrefs.GetString(CarSelection.Instance.selectedCarName + "WHEELS")  + "WHEELS_COLOR", "GREEN");

        findMyCar();

        for (int i = 0; i < wheelPanel.childCount; i++)
        {
            for (int z = 0; z < wheelPanel.GetChild(i).transform.childCount; z++)
            {
                wheelPanel.GetChild(i).transform.GetChild(z).gameObject.GetComponent<Renderer>().material.color = Color.green;
            }
        }

    }


}
