﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CarSelection : MonoBehaviour
{
    private static CarSelection _instance;
    public static CarSelection Instance { get { return _instance; } }

    public GameObject carSelectionPanel;

    public GameObject F488;
    public GameObject Supra;
    public GameObject _720s;

    public GameObject selectionMarkF488;
    public GameObject selectionMarkSupra;
    public GameObject selectionMark720s;

    public GameObject buyButton;
    public GameObject driveNormalButton;
    public GameObject driveWithTimeButton;
    public GameObject customizeButton;
    public GameObject sliderPnl;

    public GameObject selectedCar;
    public string selectedCarName;


    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    private void Start()
    {
        F488.SetActive(false);
        Supra.SetActive(false);
        _720s.SetActive(false);
        selectF488();
    }

    public void selectF488()
    {
        selectedCar = F488;
        selectedCarName = selectedCar.GetComponent<CarInfo>().carName;

        MySelectedCarAndLevel.Instance._mySelectedCar = selectedCar;

        F488.SetActive(true);
        Supra.SetActive(false);
        _720s.SetActive(false);

        selectionMarkF488.SetActive(true);
        selectionMarkSupra.SetActive(false);
        selectionMark720s.SetActive(false);

        // Set sliders according power of car
        Sliders.Instance.setSlider();

        // Car price text
        CarPriceText.Instance.setPriceText();

        buyOrDriveButtons();
    }

    public void selectSupra()
    {
        selectedCar = Supra;
        selectedCarName = selectedCar.GetComponent<CarInfo>().carName;

        MySelectedCarAndLevel.Instance._mySelectedCar = selectedCar;

        F488.SetActive(false);
        Supra.SetActive(true);
        _720s.SetActive(false);

        selectionMarkF488.SetActive(false);
        selectionMarkSupra.SetActive(true);
        selectionMark720s.SetActive(false);


        // Set sliders according power of car
        Sliders.Instance.setSlider();

        // Car price and text
        CarPriceText.Instance.setPriceText();


        buyOrDriveButtons();
    }

    public void select720s()
    {
        selectedCar = _720s;
        selectedCarName = selectedCar.GetComponent<CarInfo>().carName;

        MySelectedCarAndLevel.Instance._mySelectedCar = selectedCar;

        F488.SetActive(false);
        Supra.SetActive(false);
        _720s.SetActive(true);

        selectionMarkF488.SetActive(false);
        selectionMarkSupra.SetActive(false);
        selectionMark720s.SetActive(true);


        // Set sliders according power of car
        Sliders.Instance.setSlider();

        // Car price and text
        CarPriceText.Instance.setPriceText();


        buyOrDriveButtons();

    }


    private void buyOrDriveButtons()
    {
        if (PlayerPrefs.HasKey(selectedCar.GetComponent<CarInfo>().carName))
        {
            buyButton.SetActive(false);
            driveNormalButton.SetActive(true);
            driveWithTimeButton.SetActive(true);
            customizeButton.SetActive(true);
        }
        else
        {
            buyButton.SetActive(true);
            driveWithTimeButton.SetActive(false);
            driveNormalButton.SetActive(false);
            customizeButton.SetActive(false);
        }

    }

    public void hideAndShowButtonsForPurchasedCar()
    {
        sliderPnl.SetActive(false);
        carSelectionPanel.SetActive(false);
        buyButton.SetActive(false);
        driveWithTimeButton.SetActive(false);
        driveNormalButton.SetActive(false);
        customizeButton.SetActive(false);
    }

    public void bringBackObjectAfterPurchasedCarAnimation()
    {
        sliderPnl.SetActive(true);
        driveWithTimeButton.SetActive(true);
        driveNormalButton.SetActive(true);
        customizeButton.SetActive(true);
        carSelectionPanel.SetActive(true);
    }

}
