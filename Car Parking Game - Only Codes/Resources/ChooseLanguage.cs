﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChooseLanguage : MonoBehaviour
{

    private GameObject[] playerObjects;

    public void chooseEnglish()
    {
        findAllDontDestroyableObjects();

        PlayerPrefs.SetInt("SelectedLanguage", 0);
        PlayerPrefs.Save();
        LocalizationManager.Instance.currentLanguageID = 0;
        UnityEngine.SceneManagement.SceneManager.LoadScene("1-MENU");
    }


    public void chooseTurkish()
    {
        findAllDontDestroyableObjects();

        PlayerPrefs.SetInt("SelectedLanguage", 1);
        PlayerPrefs.Save();
        LocalizationManager.Instance.currentLanguageID = 1;
        UnityEngine.SceneManagement.SceneManager.LoadScene("1-MENU");
    }

    private void findAllDontDestroyableObjects()
    {
        GameObject[] objByTags = FindInActiveObjectsByTag("Player");

        foreach (GameObject p in objByTags)
        {
            Destroy(p);
        }
    }


    GameObject[] FindInActiveObjectsByTag(string tag)
    {
        List<GameObject> validTransforms = new List<GameObject>();
        Transform[] objs = Resources.FindObjectsOfTypeAll<Transform>() as Transform[];
        for (int i = 0; i < objs.Length; i++)
        {
            if (objs[i].hideFlags == HideFlags.None)
            {
                if (objs[i].gameObject.CompareTag(tag))
                {
                    validTransforms.Add(objs[i].gameObject);
                }
            }
        }
        return validTransforms.ToArray();
    }

}
