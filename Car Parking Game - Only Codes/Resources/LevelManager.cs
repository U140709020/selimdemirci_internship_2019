﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour
{

    private static LevelManager _instance;
    public static LevelManager Instance { get { return _instance; } }

    public GameObject awardText;
    public int dollarAwardCount;
    public int goldAwardCount;

    public GameObject timeCounterText;

    public GameObject nextLevelButton;
    public GameObject mainMenuButton;
    public GameObject backButton;
    public GameObject playAgainButton;

    public int checkPoints = 0;
    public int userLevel = 1;

    public List<GameObject> levelList = new List<GameObject>();

    public List<Vector3> spawnPointsByLevel = new List<Vector3>();
    public List<Vector3> spawnRotatesByLevel = new List<Vector3>();
    public List<int> countDownListByLevel = new List<int>();

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        if (MySelectedCarAndLevel.Instance.playAgainChecker)
        {
            if (MySelectedCarAndLevel.Instance.timeMode)
            {
                userLevel = PlayerPrefs.GetInt("UserLevelLimitedTime");
            }
            else
            {
                userLevel = PlayerPrefs.GetInt("UserLevelNormalGame");
            }
            MySelectedCarAndLevel.Instance.playAgainChecker = false;
        }
        else
        {
            userLevel = MySelectedCarAndLevel.Instance.selectedUserLevel;
        }
    }

    private void Start()
    {
        MySelectedCarAndLevel.Instance._mySelectedCar.gameObject.SetActive(false);
        levelList[userLevel-1].SetActive(true);
        MySelectedCarAndLevel.Instance._mySelectedCar.gameObject.SetActive(true);

        RCC.SetEngine(MySelectedCarAndLevel.Instance._mySelectedCar.GetComponent<RCC_CarControllerV3>(), true);

        // Check if game is time mode or not
        if (MySelectedCarAndLevel.Instance.timeMode == true)
        {
            timeCounterText.SetActive(true);
            TimeCounter.Instance.setTime();
        }
    }


    public void checkLevelFinishOrNot()
    {
        if (checkPoints >= 4)
        {
            checkPoints = 0;

            userLevel++;
            
            // Save user level according time mode or normal
            if(MySelectedCarAndLevel.Instance.timeMode == false)
            {
                if (PlayerPrefs.GetInt("UserLevelNormalGame") < userLevel)
                {
                    PlayerPrefs.SetInt("UserLevelNormalGame", userLevel);
                    PlayerPrefs.Save();
                    // Give award
                    BudgetManager.Instance.buyDollar(dollarAwardCount);
                    awardText.SetActive(true);
                    awardText.GetComponent<Text>().text = "+" + dollarAwardCount + " " + LocalizationManager.Instance.GetText("DOLLAR") + "!";
                    awardText.GetComponent<Text>().color = Color.green;
                }
            }
            else
            {
                if (PlayerPrefs.GetInt("UserLevelLimitedTime") < userLevel)
                {
                    PlayerPrefs.SetInt("UserLevelLimitedTime", userLevel);
                    PlayerPrefs.Save();
                    // Give award
                    BudgetManager.Instance.buyGold(goldAwardCount);
                    awardText.SetActive(true);
                    awardText.GetComponent<Text>().text = "+" + goldAwardCount + " " + LocalizationManager.Instance.GetText("GOLD") + "!";
                    awardText.GetComponent<Text>().color = Color.yellow;
                }

            }

            if (userLevel <= levelList.Count)
            {
                nextLevelButton.SetActive(true);
            }
            else
            {
                mainMenuButton.SetActive(true);
            }

            if(MySelectedCarAndLevel.Instance.timeMode == true)
            {
                TimeCounter.Instance.stopCountAndWin();
            }

            // Deactivate controllers, make speed ZERO and show game buttons
            MySelectedCarAndLevel.Instance._mySelectedCar.GetComponent<RCC_CarControllerV3>().speed = 0;
            RCC.SetControl(MySelectedCarAndLevel.Instance._mySelectedCar.GetComponent<RCC_CarControllerV3>(), false);


            // Hide ui elements for clean screen
            backButton.SetActive(false);

        }
    }


}
