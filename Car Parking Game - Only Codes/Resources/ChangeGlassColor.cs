﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeGlassColor : MonoBehaviour {



    private GameObject glass;


    private void findGlassObject()
    {
        glass = CarSelection.Instance.selectedCar.transform.Find("Glass").gameObject;
    }

    // Changing glass color
    public void makeGlassMagenta()
    {
        CustomizeManager.Instance.setItemInfo("Glass_magenta", 2500, "DOLLAR", CarSelection.Instance.selectedCarName + "GLASS_COLOR", "MAGENTA");

        findGlassObject();

        glass.GetComponent<Renderer>().material.color = Color.magenta;

    }


    public void makeGlassCyan()
    {
        CustomizeManager.Instance.setItemInfo("Glass_cyan", 2500, "DOLLAR", CarSelection.Instance.selectedCarName + "GLASS_COLOR", "CYAN");

        findGlassObject();

        glass.GetComponent<Renderer>().material.color = Color.cyan;

    }


}
