﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParkingSensor : MonoBehaviour
{


    public string soundName;

    private void OnTriggerEnter(Collider other)
    {
        // If reverse gear, play sound
        if(MySelectedCarAndLevel.Instance._mySelectedCar.GetComponent<RCC_CarControllerV3>().direction == -1)
        {
            SoundManager.Instance.sensorSound(soundName);
        }
        else
        {
            SoundManager.Instance.closeSensorSound();
        }
    }



}
