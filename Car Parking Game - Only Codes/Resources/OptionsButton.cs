﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OptionsButton : MonoBehaviour
{


    public GameObject optionsPopup;


    public void openOptionsPopup()
    {
        CarSelection.Instance.hideAndShowButtonsForPurchasedCar();
        GameObject go = Instantiate(optionsPopup, GameObject.Find("Canvas").transform, false);
    }

}
