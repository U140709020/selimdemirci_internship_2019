﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeCounter : MonoBehaviour
{
    private static TimeCounter _instance;
    public static TimeCounter Instance { get { return _instance; } }

    public GameObject loseText;
    public Text timeCounter;
    public int time;

    private int waitTime = 1;

    private Coroutine _coroutine;

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    private void Start()
    {
        StartCoroutine(countDown());
    }

    private IEnumerator countDown()
    {
        while(time > 0)
        {
            yield return new WaitForSeconds(1);
            time = time - waitTime;
            if(time <= 0)
            {
                waitTime = 0;
                loseText.SetActive(true);
                LevelManager.Instance.playAgainButton.SetActive(true);

                // Deactivate controllers, make speed ZERO and show game buttons
                MySelectedCarAndLevel.Instance._mySelectedCar.GetComponent<RCC_CarControllerV3>().speed = 0;
                RCC.SetControl(MySelectedCarAndLevel.Instance._mySelectedCar.GetComponent<RCC_CarControllerV3>(), false);
            }

            timeCounter.text = time.ToString();
        }
    }

    public void setTime()
    {
        time = LevelManager.Instance.countDownListByLevel[LevelManager.Instance.userLevel-1];
        timeCounter.text = time.ToString();
        waitTime = 1;
    }

    public void stopCountAndWin()
    {
        waitTime = 0;
    }

}
