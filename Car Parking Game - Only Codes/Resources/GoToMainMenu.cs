﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GoToMainMenu : MonoBehaviour
{

    public void goMainMenu()
    {
        findAllDontDestroyableObjects();
        SoundManager.Instance.closeSensorSound();
        SceneManager.LoadScene("1-MENU");
    }


    // Destroy all player objects including DontDestroyable too (They will recreate in main menu)
    private void findAllDontDestroyableObjects()
    {
        GameObject[] objByTags = FindInActiveObjectsByTag("Player");

        foreach (GameObject p in objByTags)
        {
            Destroy(p);
        }
    }


    GameObject[] FindInActiveObjectsByTag(string tag)
    {
        List<GameObject> validTransforms = new List<GameObject>();
        Transform[] objs = Resources.FindObjectsOfTypeAll<Transform>() as Transform[];
        for (int i = 0; i < objs.Length; i++)
        {
            if (objs[i].hideFlags == HideFlags.None)
            {
                if (objs[i].gameObject.CompareTag(tag))
                {
                    validTransforms.Add(objs[i].gameObject);
                }
            }
        }
        return validTransforms.ToArray();
    }

}
