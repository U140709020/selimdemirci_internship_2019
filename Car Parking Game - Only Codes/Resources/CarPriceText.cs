﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CarPriceText : MonoBehaviour
{
    private static CarPriceText _instance;
    public static CarPriceText Instance { get { return _instance; } }


    public Text priceText;


    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }


    public void setPriceText()
    {
        if (CarSelection.Instance.selectedCar.GetComponent<CarInfo>().priceCurrency == "GOLD")
        {
            priceText.text = CarSelection.Instance.selectedCar.GetComponent<CarInfo>().carPrice.ToString() + " " + LocalizationManager.Instance.GetText("GOLD");
            priceText.color = Color.yellow;
        }
        else if (CarSelection.Instance.selectedCar.GetComponent<CarInfo>().priceCurrency == "DOLLAR")
        {
            priceText.text = CarSelection.Instance.selectedCar.GetComponent<CarInfo>().carPrice.ToString() + " " + LocalizationManager.Instance.GetText("DOLLAR");
            priceText.color = Color.green;
        }
        else
        {
            priceText.text = "";
        }

        closeTextForPurchasedCars();

    }

    public void closeTextForPurchasedCars()
    {
        if (PlayerPrefs.HasKey(CarSelection.Instance.selectedCar.GetComponent<CarInfo>().carName))
        {
            priceText.text = "";
        }
    }



}
