﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MySelectedCarAndLevel : MonoBehaviour
{
    private static MySelectedCarAndLevel _instance;
    public static MySelectedCarAndLevel Instance { get { return _instance; } }

    // This script transfer car object and time mode informations from main menu to city scene

    public GameObject _mySelectedCar;
    public int selectedUserLevel;
    public bool timeMode;
    public bool playAgainChecker = false;

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    private void Start()
    {
        DontDestroyOnLoad(this);
    }


}
