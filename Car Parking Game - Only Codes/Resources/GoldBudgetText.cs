﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GoldBudgetText : MonoBehaviour
{
    private static GoldBudgetText _instance;
    public static GoldBudgetText Instance { get { return _instance; } }


    public Text goldBudgetText;

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    void Start()
    {
        showGoldBudget();
    }

    public void showGoldBudget()
    {
        goldBudgetText.text = BudgetManager.Instance.goldBudget + " " + LocalizationManager.Instance.GetText("GOLD");
    }
}
