﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayAgainButton : MonoBehaviour
{

    public void playAgain()
    {
        MySelectedCarAndLevel.Instance.playAgainChecker = true;
        RCC.SetControl(MySelectedCarAndLevel.Instance._mySelectedCar.GetComponent<RCC_CarControllerV3>(), true);
        UnityEngine.SceneManagement.SceneManager.LoadScene("TrafficRulesDemoSceneNight");
    }

}
