﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NextLevelButton : MonoBehaviour
{


    public void goToNextLevel()
    {
        // Loading next level
        LevelManager.Instance.levelList[LevelManager.Instance.userLevel - 2].SetActive(false);
        LevelManager.Instance.levelList[LevelManager.Instance.userLevel - 1].SetActive(true);
        LevelManager.Instance.awardText.SetActive(false);
        LevelManager.Instance.backButton.SetActive(true);

        // If time mode is open, set the time again
        if (MySelectedCarAndLevel.Instance.timeMode == true)
        {
            TimeCounter.Instance.setTime();
        }

        // Activate controllers again
        RCC.SetControl(MySelectedCarAndLevel.Instance._mySelectedCar.GetComponent<RCC_CarControllerV3>(), true);

        gameObject.SetActive(false);
    }

}
