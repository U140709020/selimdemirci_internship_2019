﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Sliders : MonoBehaviour
{
    private static Sliders _instance;
    public static Sliders Instance { get { return _instance; } }


    public Slider speedSlider;
    public Slider accelerationSlider;
    public Slider breakSlider;

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    // Set sliders according car's info
    public void setSlider()
    {
        speedSlider.value = CarSelection.Instance.selectedCar.GetComponent<CarInfo>().speed;
        accelerationSlider.value = CarSelection.Instance.selectedCar.GetComponent<CarInfo>().acceleration;
        breakSlider.value = CarSelection.Instance.selectedCar.GetComponent<CarInfo>().breakC;

    }

}
