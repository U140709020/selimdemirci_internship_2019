﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Friend : MonoBehaviour
{

    private static Friend _instance;
    public static Friend Instance { get { return _instance; } }


    public string friendName;
    public string friendUserId;


    public GameObject friendPopup;



    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    [PunRPC]
    void addFriend(string _myNickName, string _myUserId)
    {
        GameObject go = Instantiate(friendPopup, GameObject.Find("Canvas").transform, false);
        go.GetComponentInChildren<Text>().text = _myNickName + " arkadaşlık isteği gönderdi.";
        go.transform.GetChild(3).gameObject.SetActive(false);

        friendName = _myNickName;
        friendUserId = _myUserId;
    }


    [PunRPC]
    void acceptFriend(string _playerName, string _playerUserId)
    {
        GameObject go = Instantiate(friendPopup, GameObject.Find("Canvas").transform, false);
        go.GetComponentInChildren<Text>().text = _playerName + " arkadaşlık isteğini kabul etti.";
        go.transform.GetChild(1).gameObject.SetActive(false);
        go.transform.GetChild(2).gameObject.SetActive(false);

        PlayerPrefs.SetString("Friend", _playerUserId);
        PlayerPrefs.Save();

        RefreshFriendList.Instance.refreshList();
    }

    [PunRPC]
    void declineFriend(string _myNickName)
    {
        GameObject go = Instantiate(friendPopup, GameObject.Find("Canvas").transform, false);
        go.GetComponentInChildren<Text>().text = _myNickName + " arkadaşlık isteğini reddetti.";
        go.transform.GetChild(1).gameObject.SetActive(false);
        go.transform.GetChild(2).gameObject.SetActive(false);


    }
}
