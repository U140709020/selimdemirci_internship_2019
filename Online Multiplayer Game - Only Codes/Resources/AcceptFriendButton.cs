﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AcceptFriendButton : MonoBehaviour
{




    public void acceptFriendRequest()
    {
        PlayerPrefs.SetString("Friend", Friend.Instance.friendUserId);
        PlayerPrefs.Save();

        PhotonView pw = GameObject.Find("Canvas/PlayerList").GetComponent<PhotonView>();

        foreach (PhotonPlayer p in PhotonNetwork.playerList)
        {
            if (p.NickName == Friend.Instance.friendName)
            {
                pw.RPC("acceptFriend", p, PhotonNetwork.player.NickName, PhotonNetwork.player.UserId);
            }
        }
        RefreshFriendList.Instance.refreshList();
        Destroy(gameObject.transform.parent.gameObject);
    }

}
