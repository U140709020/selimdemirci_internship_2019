﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddFriendButton : MonoBehaviour
{



    public string playerName;
    public string myUserId;
    public string myNickName;
    public GameObject friendPopup;

    public void addThisPlayerAsMyFriend()
    {
        PhotonView pw = GameObject.Find("Canvas/PlayerList").GetComponent<PhotonView>();
        
        foreach(PhotonPlayer p in PhotonNetwork.playerList)
        {
            if(p.NickName == playerName)
            {
                pw.RPC("addFriend", p, PhotonNetwork.player.NickName, PhotonNetwork.player.UserId);
            }
        }
    }

}
