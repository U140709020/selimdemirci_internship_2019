﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeclineFriendButton : MonoBehaviour
{


    public void declineFriendRequest()
    {
        PhotonView pw = GameObject.Find("Canvas/PlayerList").GetComponent<PhotonView>();

        foreach (PhotonPlayer p in PhotonNetwork.playerList)
        {
            if (p.NickName == Friend.Instance.friendName)
            {
                pw.RPC("declineFriend", p, PhotonNetwork.player.NickName);
            }
        }

        Destroy(gameObject.transform.parent.gameObject);
    }
}
