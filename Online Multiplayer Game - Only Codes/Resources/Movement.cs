﻿using UnityEngine;



public class Movement : Photon.MonoBehaviour
{

    private float speed = 10f;
    private float rotateSpeed = 100f;

    public bool isGrounded;
    public GameObject emoji;
    public bool emojiTimeChecker = false;

    public Sprite punch;
    public Sprite youWin;
    public int spriteIndex;


    Vector3 realPosition = Vector3.zero;
    Quaternion realRotation = Quaternion.identity;


    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == ("Ground") && isGrounded == false)
        {
            isGrounded = true;
        }
    }

    private void Awake()
    {
        // PhotonNetwork.sendRate = 60;
        // PhotonNetwork.sendRateOnSerialize = 30;
    }


    void Update()
    {
        if (photonView.isMine)
        {
            transform.Translate(0f, 0f, speed * Input.GetAxis("Vertical") * Time.deltaTime);
            transform.Rotate(0f, Input.GetAxis("Horizontal") * rotateSpeed * Time.deltaTime, 0f);


            if (Input.GetKeyDown(KeyCode.Space) && isGrounded)
            {
                gameObject.GetComponent<Rigidbody>().AddForce(new Vector3(0, 10, 0), ForceMode.Impulse);
                isGrounded = false;
            }

            if (Input.GetKeyDown(KeyCode.Alpha1) && emojiTimeChecker == false)
            {
                spriteIndex = 1;
                photonView.RPC("changeSprite", PhotonTargets.All, new object[] { spriteIndex });
                emojiTimeChecker = true;
            }

            if (Input.GetKeyDown(KeyCode.Alpha2) && emojiTimeChecker == false)
            {
                spriteIndex = 2;
                photonView.RPC("changeSprite", PhotonTargets.All, new object[] { spriteIndex });
                emojiTimeChecker = true;

                // Debug.Log(PhotonNetwork.FindFriends(new string[] { "af9294079ca812c0d96ff68c85d1a787f56bac01" }).ToString());
            }

        }
        else
        {
            transform.position = Vector3.Lerp(transform.position, realPosition, Time.deltaTime * 5);
            transform.rotation = Quaternion.Lerp(transform.rotation, realRotation, Time.deltaTime * 5);
        }
    }


    [PunRPC]
    void changeSprite(int _spriteIndex)
    {
        emoji.SetActive(true);
        emoji.transform.GetComponentInChildren<ParticleSystem>().Play();
        if(_spriteIndex == 1)
        {
            emoji.transform.GetComponentInChildren<ParticleSystem>().textureSheetAnimation.SetSprite(0, punch);
        }
        else if(_spriteIndex == 2)
        {
            emoji.transform.GetComponentInChildren<ParticleSystem>().textureSheetAnimation.SetSprite(0, youWin);
        }
        Invoke("showForAWhile", 3f);
    }

    void showForAWhile()
    {
        emoji.SetActive(false);
        emojiTimeChecker = false;
    }


    void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            stream.SendNext(transform.position);
            stream.SendNext(transform.rotation);
        }
        else
        {
            realPosition = (Vector3)stream.ReceiveNext();
            realRotation = (Quaternion)stream.ReceiveNext();
        }
    }
}
