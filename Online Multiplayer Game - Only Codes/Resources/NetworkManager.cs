﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class NetworkManager : Photon.PunBehaviour
{

    public GameObject playerInfoObject;

    public GameObject lobbyCam;

    public Transform spawnPoint;

    public GameObject lobbyUI;

    public Text statusText;

    public const string Version = "1.0";
    public const string RoomName = "Multiplayer";

    public string playerPrefabName = "Player";


    private void Start()
    {
        PhotonNetwork.ConnectUsingSettings(Version);
        PhotonNetwork.AuthValues = new AuthenticationValues(SystemInfo.deviceUniqueIdentifier);
        //PhotonNetwork.AuthValues = new AuthenticationValues("Selim");
        PhotonNetwork.playerName = PhotonNetwork.AuthValues.UserId;
    }

    private void Update()
    {
        statusText.text = PhotonNetwork.connectionStateDetailed.ToString();
    }


    public override void OnConnectionFail(DisconnectCause cause)
    {
        print("Connection failed: " + cause.ToString());
    }

    public override void OnJoinedLobby()
    {
        RoomOptions roomOptions = new RoomOptions() { IsVisible = true, MaxPlayers = 2 };
        roomOptions.PublishUserId = true;
        PhotonNetwork.JoinOrCreateRoom(RoomName, roomOptions, TypedLobby.Default);
    }

    public override void OnJoinedRoom()
    {
        lobbyCam.SetActive(false);
        lobbyUI.SetActive(false);
        GameObject player = PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "Player"), spawnPoint.position, spawnPoint.rotation, 0);
        refreshPlayerList();
    }

    public override void OnPhotonPlayerConnected(PhotonPlayer newPlayer)
    {
        refreshPlayerList();
        print("New player connected.");
    }

    public override void OnPhotonPlayerDisconnected(PhotonPlayer otherPlayer)
    {
        refreshPlayerList();
        print("New player disconnected.");
    }




    public void refreshPlayerList()
    {
        foreach (Transform t in GameObject.Find("Canvas/PlayerList").transform)
        {
            Destroy(t.gameObject);
        }


        foreach (PhotonPlayer p in PhotonNetwork.otherPlayers)
        {
            GameObject go = Instantiate(playerInfoObject, GameObject.Find("Canvas/PlayerList").transform, false);
            go.GetComponentInChildren<Text>().text = p.NickName;
            go.GetComponent<AddFriendButton>().myUserId = SystemInfo.deviceUniqueIdentifier;
            go.GetComponent<AddFriendButton>().playerName = p.NickName;
        }
    }
}
