﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RefreshFriendList : MonoBehaviour
{

    private static RefreshFriendList _instance;
    public static RefreshFriendList Instance { get { return _instance; } }

    public Text friendList;


    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }


    private void Start()
    {
        refreshList();
    }

    public void refreshList()
    {
        friendList.text = PlayerPrefs.GetString("Friend");
    }
}
