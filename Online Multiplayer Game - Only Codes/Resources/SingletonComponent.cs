﻿using UnityEngine;

public abstract class SingletonComponent<T> : MonoBehaviour where T : MonoBehaviour
{
    public bool _isReady = false;
    private static T _instance = null;

    public virtual bool IsReady()
    {
        return _isReady;
    }
    /// <summary>
    /// Gets the instance.
    /// </summary>
    /// <value>The instance.</value>
    public static T Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = (T)FindObjectOfType(typeof(T));
                if (_instance == null)
                {
                    string goName = "_" + typeof(T).ToString();

                    GameObject go = GameObject.Find(goName);
                    if (go == null)
                    {
                        go = new GameObject();
                        go.name = goName;
                    }

                    _instance = go.AddComponent<T>();
                }
            }
            return _instance;
        }
    }

    protected virtual void Awake()
    {
        if (Instance == null)
        {
            Debug.LogError("Awake Error -->", this);
        }
    }

    protected void OnDestroy()
    {
        _instance = null;
    }

    public static bool IsManagerActive()
    {
        return _instance != null;
    }

    public virtual WaitUntil WaitForActivation()
    {
        return new WaitUntil(() => IsReady());
    }
}