﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClosePopup : MonoBehaviour
{

    public void destroyThisPopup()
    {
        Destroy(gameObject.transform.parent.gameObject);
    }

}
