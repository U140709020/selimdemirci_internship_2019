﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkPlayer : Photon.MonoBehaviour
{

    public GameObject localCam;

    private void Start()
    {
        if (!photonView.isMine)
        {
            localCam.SetActive(false);
        }
    }

}
